import { Context, Middleware } from 'koa';

export const deadEnd: Middleware = async function deadEnd(ctx: Context) {
  ctx.status = 404;
  ctx.body = { message: 'not found' };
};
