import { Context, Middleware } from 'koa';
import { Update } from '../model';
import { simpleRequest } from '../requestWrapper';
import { CHAT_ID } from '../env';

export const handleUpdate: Middleware = async function handleUpdate(ctx: Context, next) {
  const update: Update = ctx.request.body;
  if (
    update.message
    && update.message.chat.id === CHAT_ID
    && (update.message.new_chat_members || update.message.left_chat_member)
  ) {
    const messageId = update.message.message_id;
    simpleRequest('deleteMessage', {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        chat_id: CHAT_ID,
        message_id: messageId,
      }),
    });
  }
  await next();
  ctx.status = 200;
  ctx.body = { message: 'ok' };
};
