export const {
  TELEGRAM_BOT_TOKEN, PORT, HOST = '0.0.0.0',
} = process.env;

export const CHAT_ID = +(process.env.CHAT_ID as string);
