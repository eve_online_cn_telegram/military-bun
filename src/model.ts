export type TelegramBotAPIResponse = TelegramBotAPISuccessResponse | TelegramBotAPIFailResponse

export interface TelegramBotAPISuccessResponse {
  ok: true;
  description?: string;
  result?: any;
}

export interface TelegramBotAPIFailResponse {
  ok: false;
  description?: string;
  error_code?: number;
  parameters?: ResponseParameters;
}

export interface ResponseParameters {
  migrate_to_chat_id?: number;
  retry_after?: number;
}

export interface Update {
  update_id: number;
  message?: Message;
  edited_message?: Message;
  channel_post?: Message;
  edited_channel_post?: Message;
  inline_query?: UndefinedType;
  chosen_inline_result?: UndefinedType;
  callback_query?: UndefinedType;
  shipping_query?: UndefinedType;
  pre_checkout_query?: UndefinedType;
  poll?: UndefinedType;
  poll_answer?: UndefinedType;
}

export interface Message {
  message_id: number;
  from?: User;
  date: number;
  chat: Chat;
  forward_from?: User;
  forward_from_chat?: Chat;
  forward_from_message_id?: number;
  forward_signature?: string;
  forward_sender_name?: string;
  forward_date?: number;
  reply_to_message?: Message;
  edit_date?: number;
  media_group_id?: string;
  author_signature?: string;
  text?: string;
  entities?: UndefinedType[];
  caption_entities?: UndefinedType[];
  audio?: UndefinedType;
  document?: UndefinedType;
  animation?: UndefinedType;
  game?: UndefinedType;
  photo?: UndefinedType[];
  sticker?: UndefinedType;
  video?: UndefinedType;
  voice?: UndefinedType;
  video_note?: UndefinedType;
  caption?: UndefinedType;
  contact?: UndefinedType;
  location?: UndefinedType;
  venue?: UndefinedType;
  poll?: UndefinedType;
  dice?: UndefinedType;
  new_chat_members?: User[];
  left_chat_member?: User;
  new_chat_title?: string;
  new_chat_photo?: UndefinedType[];
  delete_chat_photo?: boolean;
  group_chat_created?: boolean;
  supergroup_chat_created?: boolean;
  channel_chat_created?: boolean;
  migrate_to_chat_id?: number;
  migrate_from_chat_id?: number;
  pinned_message?: Message;
  invoice?: UndefinedType;
  successful_payment?: UndefinedType;
  connected_website?: string;
  passport_data?: UndefinedType;
  reply_markup?: UndefinedType;
}

export interface User {
  id: number;
  is_bot: boolean;
  first_name: string;
  last_name?: string;
  username?: string;
  language_code?: string;
  can_join_groups?: boolean;
  can_read_all_group_messages?: boolean;
  supports_inline_queries?: boolean;
}

export interface Chat {
  id: number;
  type: string;
  title?: string;
  username?: string;
  first_name?: string;
  last_name?: string;
  photo?: UndefinedType;
  description?: string;
  invite_link?: string;
  pinned_message?: Message;
  permissions?: UndefinedType;
  slow_mode_delay?: number;
  sticker_set_name?: string;
  can_set_sticker_set?: boolean;
}

type UndefinedType = any
