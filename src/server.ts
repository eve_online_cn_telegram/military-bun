import Koa from 'koa';
import http from 'http';
import bodyParser from 'koa-bodyparser';
import { PORT, HOST } from './env';
import { deadEnd } from './middleware/deadEnd';
import { handleUpdate } from './middleware/handleUPdate';

export const app = new Koa();

export const server = http.createServer(app.callback());

app.use(bodyParser());
app.use(handleUpdate);
app.use(deadEnd);

export function start(): void {
  server.listen({ port: PORT, host: HOST });
}
