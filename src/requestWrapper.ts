import fetch from 'node-fetch';
import { TelegramBotAPIResponse } from './model';
import { TELEGRAM_BOT_TOKEN } from './env';

export const endPoint = 'https://api.telegram.org/bot';

export async function request(apiMethod: string, ...args: any[]): Promise<TelegramBotAPIResponse> {
  const res = await fetch(`${endPoint + TELEGRAM_BOT_TOKEN}/${apiMethod}`, ...args);
  const json = await res.json();
  return json;
}

export async function simpleRequest(apiMethod: string, ...args: any[]): Promise<any> {
  const json = await request(apiMethod, ...args);
  if (json.ok) return json.result;
  throw new Error(json.description);
}
